import { fighterService } from './typescript/services/fightersService';
import { IFighter } from  './typescript/interfaces/IFighter';
import { createFighters } from './typescript/components/fightersView';

class App { 
    constructor() {
        startApp();
    }
    static rootElement = <HTMLElement>document.getElementById('root');
    static loadingElement = <HTMLElement> document.getElementById('loading-overlay');
}

const startApp = async () : Promise<void>  => {
    try {
        if(App.loadingElement) {
            App.loadingElement.style.visibility = 'visible';
        }

      const fighters = <Array<IFighter>> await fighterService.getFighters();
      const fightersElement = <HTMLElement> createFighters(fighters);
      if (App.rootElement)
        App.rootElement.appendChild(fightersElement);

      
    } catch (error) {
      console.warn(error);
      if (App.rootElement) {
        App.rootElement.innerText = 'Failed to load data';
      }
    } finally {
        if(App.loadingElement) {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
  }

  export default App;