export enum controls {
    PlayerOneAttack = 'KeyA',
    PlayerOneBlock = 'KeyD',
    PlayerTwoAttack = 'KeyJ',
    PlayerTwoBlock = 'KeyL'
    // PlayerOneCriticalHitCombination = ['KeyQ', 'KeyW', 'KeyE'],
    // PlayerTwoCriticalHitCombination = ['KeyU', 'KeyI', 'KeyO']
  }

// export let multuplyControls = new Set<string[]>();
// multuplyControls.add(['KeyQ', 'KeyW', 'KeyE']);
// multuplyControls.add(['KeyU', 'KeyI', 'KeyO']);
export const PlayerOneCriticalHitCombination = ['KeyQ', 'KeyW', 'KeyE'];
export const PlayerTwoCriticalHitCombination = ['KeyU', 'KeyI', 'KeyO'];
