import { IFighter } from "../interfaces/IFighter"

export class Fighter implements IFighter {
    name: string;
    source: string;
    id: string;

    constructor(id: string, name:string, source: string, ) {
        this.name = name;
        this.source = source;
        this.id = id;
    }
}