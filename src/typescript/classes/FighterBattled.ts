import { FighterDetailed } from "./FighterDetailed";
import { IFighterBattled } from "../interfaces/IFighterBattled";
import { updateHealthIndicator, updateStatusInfo } from '../components/arena'; //setTimer, updateTimer

export class FighterBattled extends FighterDetailed implements IFighterBattled {
    initialHealth: number;
    blocked: boolean;
    availableFatalAttack: boolean;
    position: 'right' | "left";
    countOfAttacks: number;
    countOfFatalAttack: number;
    countOfFaults: number;
    countOfDodges: number;

    // constructor(name:string, attack: number, defense: number, health: number, source: string, id: number, position: fighterPosition) {
    constructor(fighter: FighterDetailed, position: 'right' | "left") {
        super(fighter.id, fighter.name, fighter.source, fighter.attack, fighter.defense, fighter.health);
        this.initialHealth = fighter.health; // rename to currentHealth, change it in functions
        this.blocked = false;
        this.availableFatalAttack= true;
        this.position = position;
        this.countOfAttacks = 0;
        this.countOfDodges = 0;
        this.countOfFatalAttack = 0;
        this.countOfFaults = 0;
    }

    attackAction(defender: IFighterBattled) : void {
        if(!this.blocked) {
          const damage = (defender.blocked) ? 0: this.getDamage(defender); //(defender.blocked) ? defender.getBlockPower() : 0;
          if (damage == 0){ 
            updateStatusInfo(defender, 'Dodged a hit!');
            console.log(`updateStatusInfo(${defender}, 'Dodged a hit!')`);
            defender.countOfDodges++;
          }
          else {
            this.countOfAttacks++;
            defender.countOfFaults++;
            updateHealthIndicator(defender, damage);
            console.log(`updateHealthIndicator(${defender}, ${damage})`);
            const roundedDamage = this.getRoundedDamage(damage);
            defender.health -= damage;
            const massage = (defender.health >= 0) ? `- ${roundedDamage} of health` : `Game over! :(`;
            updateStatusInfo(defender, massage);
            console.log(`updateStatusInfo(${defender}, ${massage})`);
          }
        }
        else {
          updateStatusInfo(this, 'Unblock for attack');
          console.log(`updateStatusInfo(${defender}, 'Unblock for attack')`);
        }
    }

    blockAction() : void {
        this.blocked = !this.blocked;
        const blockedText = this.blocked ? "Under block" : "";
        updateStatusInfo(this, blockedText);
        console.log(`updateStatusInfo(${this}, ${blockedText})`);
    }

    getDamage(defender: IFighterBattled) : number {
        const blockPowerValue = defender.getBlockPower(); // error is here
        const hitPowerValue = this.getHitPower();
        const damage = blockPowerValue >= hitPowerValue ? 0 : hitPowerValue - blockPowerValue;
        return damage;
    }

    getRoundedDamage(damage: number): number {
        return Math.round(damage * 100) / 100;
    }
      
    getFatalDamage() : number {
        return 2 * this.attack;
    }
      
    getHitPower() : number {
        const criticalHitChance = Math.random() + 1;
        return criticalHitChance * this.attack;
    }
      
    getBlockPower() : number {
        const dodgeChance = Math.random() + 1;
        return dodgeChance * this.defense;
    }

   async fatalAttack(defender: IFighterBattled) : Promise<void> {
        if(!this.blocked){
          this.countOfFatalAttack++;
          defender.countOfFaults++;
          const damage = this.getFatalDamage();
          updateHealthIndicator(defender, damage);
          const roundedDamage = defender.getRoundedDamage(damage);
          defender.health -= damage;
          const massage = (defender.health > 0) ? `Damn it man! It is fatal attack - ${roundedDamage} of health` : `Game over! :(`;
          updateStatusInfo(defender, massage);
        }
        else 
          updateStatusInfo(this, 'Unblock for attack');
      }
} 