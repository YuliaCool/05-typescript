import { IFighterDetailed } from "../interfaces/IFighterDetailed";
import { Fighter } from "./Fighter";

export class FighterDetailed extends Fighter implements IFighterDetailed {
    attack: number;
    defense: number;
    health: number;

    constructor(id: string, name:string, source: string, attack: number, defense: number, health: number) {
        super(id, name, source);
        this.attack = attack;
        this.defense = defense;
        this.health = health;
    }
}