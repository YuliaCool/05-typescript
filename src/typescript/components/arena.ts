import { IFighterDetailed } from '../interfaces/IFighterDetailed';
import { IFighterBattled } from '../interfaces/IFighterBattled';
import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { goToPreview } from './modal/winner';
import { showWinnerModal } from '../components/modal/winner';

const disabledTime = 10;

export function renderArena(selectedFighters: Array<IFighterDetailed>) : void {
    const root = <HTMLElement>document.getElementById('root');
    const arena = createArena(selectedFighters);
  
    root.innerHTML = '';
    root.append(arena);
  
   fight(selectedFighters[0], selectedFighters[1])
   .then( (result:IFighterBattled) => {
      showWinnerModal(result);
      //alert(result);
   })
   .catch((error: Error) => console.log(error.message));
  }

  function createHealthIndicators(leftFighter: IFighterDetailed, rightFighter: IFighterDetailed) : HTMLElement {
    const healthIndicators = <HTMLElement> createElement({ tagName: 'div', className: 'arena___fight-status' });
    const versusSign = <HTMLElement> createElement({ tagName: 'div', className: 'arena___versus-sign' });
    const leftFighterIndicator = <HTMLElement> createHealthIndicator(leftFighter, 'left');
    const rightFighterIndicator = <HTMLElement> createHealthIndicator(rightFighter, 'right');
  
    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
  }

  function createArena(selectedFighters: IFighterDetailed[]) : HTMLElement {
    const arena = <HTMLElement> createElement({ tagName: 'div', className: 'arena___root' });
    // according to issue https://github.com/microsoft/TypeScript/issues/26350 spread operator doens't worrect work in current version
    const healthIndicators = <HTMLElement> createHealthIndicators(selectedFighters[0], selectedFighters[1]);

    const fighters = <HTMLElement> createFighters(selectedFighters[0], selectedFighters[1]);
    const backButton = <HTMLElement> createBackButton();
    
    arena.append(backButton, healthIndicators, fighters);
    return arena;
  }

  function createHealthIndicator(fighter: IFighterDetailed, position: 'left' | 'right') : HTMLElement {
    const { name } = fighter;
    const container = <HTMLElement> createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    const fighterName = <HTMLElement> createElement({ tagName: 'span', className: 'arena___fighter-name' });
    const indicator = <HTMLElement> createElement({ tagName: 'div', className: 'arena___health-indicator' });
    const bar = <HTMLElement> createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});
    const statusInfoWrapper = <HTMLElement> createElement({ tagName: 'div', className: 'arena___fighter-info-wrapper' });
    const statusInfo = <HTMLElement> createElement({tagName: 'span', className: 'arena___fighter-info', attributes: {id: `${position}-fighter-info` }});
    const fatalAttackTimerStatus = <HTMLElement> createElement({tagName: 'span', className: 'arena___fighter-timer', attributes: {id: `${position}-fighter-timer-status` }});
    const fatalAttackTimer = <HTMLElement> createElement({tagName: 'span', className: 'arena___fighter-timer', attributes: {id: `${position}-fighter-timer` }});
  
    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);
  
    statusInfoWrapper.append(statusInfo);
    statusInfoWrapper.appendChild(document.createElement("br"));
    statusInfoWrapper.append(fatalAttackTimerStatus);
    statusInfoWrapper.append(fatalAttackTimer);
    container.append(statusInfoWrapper);
  
    return container;
  }

  function createFighters(firstFighter: IFighterDetailed, secondFighter: IFighterDetailed) : HTMLElement {
    const battleField = <HTMLElement> createElement({ tagName: 'div', className: `arena___battlefield` });
    const firstFighterElement = <HTMLElement> createFighter(firstFighter, 'left');
    const secondFighterElement = <HTMLElement> createFighter(secondFighter, 'right');
  
    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
  }

  function createFighter(fighter: IFighterDetailed, position: 'left' | 'right') : HTMLElement {
    const imgElement = <HTMLElement>createFighterImage(fighter);
    const positionClassName : string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    const fighterElement = <HTMLElement> createElement({
      tagName: 'div',
      className: `arena___fighter ${positionClassName}`,
    });
  
    fighterElement.append(imgElement);
    return fighterElement;
  }

  function createBackButton() : HTMLElement {
    const container = <HTMLElement> createElement({ tagName: 'div', className: 'arena___back-btn-wrapper' });
    const backBtn = <HTMLElement> createElement({
      tagName: 'button',
      className: 'arena___back-btn',
    });
  
    const onClick = goToPreview;
    backBtn.addEventListener('click', onClick, false);
    backBtn.innerText = 'Back';
    container.append(backBtn);
    return container;
  }

  export function updateHealthIndicator(fighter:IFighterBattled, damage:number) : void {
    const healthBarId = fighter.position + "-fighter-indicator";
    const currentHealthBarId = <HTMLElement>document.getElementById(healthBarId);
    let currentWidth: string | null = currentHealthBarId.style.width;
    let currentWidthPersents: number;
    let updatedWidth: number;
    if (!currentWidth) 
      currentWidthPersents = 100;
    else 
      currentWidthPersents = parseFloat(currentWidth);
  
    if (fighter.health > 0) {
      let damageInPersent = (damage * 100) / fighter.initialHealth;
      updatedWidth = currentWidthPersents - damageInPersent;
      if (updatedWidth < 0) 
        updatedWidth = 0;
    }
    else updatedWidth = 0;
    
    currentHealthBarId.style.width = updatedWidth + "%";
  }

  export function updateStatusInfo(fighter:IFighterBattled, text:string) : void {
    const id = `${fighter.position}-fighter-info`;
    const domItem = <HTMLElement>document.getElementById(id);
    domItem.innerText = text;
  }
  
  export function setTimer(fatalAttacker: IFighterBattled, textStatus = "Fatal attack will be available in "){
    const idTimer: string = `${fatalAttacker.position}-fighter-timer`;
    const domItemTimer = <HTMLElement> document.getElementById(idTimer);
    domItemTimer.innerText = disabledTime.toString();
  
    const idTimerStatus: string = `${fatalAttacker.position}-fighter-timer-status`;
    const domItemTimerStatus = <HTMLElement> document.getElementById(idTimerStatus);
    domItemTimerStatus.innerText = textStatus;
  }
  
  function resetTimer(fatalAttacker: IFighterBattled, textStatus = "Fatal attack is available"){
    const id = `${fatalAttacker.position}-fighter-timer`;
    const domItemTimer = <HTMLElement> document.getElementById(id);
    domItemTimer.innerText = '';
  
    const idTimerStatus = `${fatalAttacker.position}-fighter-timer-status`;
    const domItemTimerStatus = <HTMLElement> document.getElementById(idTimerStatus);
    domItemTimerStatus.innerText = textStatus;
  }
  
  export async function updateTimer(fatalAttacker: IFighterBattled){
    const timerId = setInterval(function(){
      const id = `${fatalAttacker.position}-fighter-timer`;
      const domItemTimer = <HTMLElement> document.getElementById(id);
      const currentSecond = parseInt(domItemTimer.innerHTML);
      domItemTimer.innerHTML = (currentSecond - 1).toString();
      if (currentSecond == 0){
        clearInterval(timerId);
        resetTimer(fatalAttacker);
        fatalAttacker.availableFatalAttack = true;
      }
    }, 1000);
  }