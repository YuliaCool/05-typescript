import { IFighterDetailed } from '../interfaces/IFighterDetailed';
import { IFighterBattled } from '../interfaces/IFighterBattled';
import { FighterDetailed } from "../classes/FighterDetailed";
import { FighterBattled } from '../classes/FighterBattled';
import { controls, PlayerOneCriticalHitCombination, PlayerTwoCriticalHitCombination } from '../../constants/controls';
import { setTimer, updateTimer } from './arena';

export const fight = (firstFighter:IFighterDetailed, secondFighter:IFighterDetailed) => {
    return new Promise<IFighterBattled>((resolve) => {
        let keysPressed = new Set<string>();
        const firstFighterOutfitted = new FighterBattled(firstFighter, "left");
        const secondFighterOutfitted = new FighterBattled(secondFighter, "right");
        document.addEventListener('keydown', handleAttack);
        document.addEventListener('keyup', handleFatalAttack);

        function handleAttack(e: KeyboardEvent){
            const key = e.code;
            switch(key){
              case controls.PlayerOneAttack: {
                firstFighterOutfitted.attackAction(secondFighterOutfitted); 
                break;
              } 
              case controls.PlayerTwoAttack: {
                secondFighterOutfitted.attackAction(firstFighterOutfitted);
                break;
              }
              case controls.PlayerOneBlock: {
                firstFighterOutfitted.blockAction();
                break;
              }
              case controls.PlayerTwoBlock: {
                secondFighterOutfitted.blockAction();
                break;
              }
            }

            if (firstFighterOutfitted.health <= 0 || secondFighterOutfitted.health <= 0) {
                document.removeEventListener('keydown', handleAttack);
                document.removeEventListener('keyup', handleFatalAttack);
                if (firstFighterOutfitted.health <= 0) resolve(secondFighterOutfitted);
                else resolve(firstFighterOutfitted);
            }
        }
        
      async function handleFatalAttack(e: KeyboardEvent){
        keysPressed.add(e.code);
        let fatalAttacker:IFighterBattled, fatalDefender: IFighterBattled;

        let firstFighterAttacks = checkCombination(PlayerOneCriticalHitCombination, keysPressed);
        let secondFighterAttacks = checkCombination(PlayerTwoCriticalHitCombination, keysPressed);

        if ((firstFighterAttacks && firstFighterOutfitted.availableFatalAttack) 
        || (secondFighterAttacks && secondFighterOutfitted.availableFatalAttack)){

          fatalAttacker = (firstFighterAttacks && firstFighterOutfitted.availableFatalAttack) ? firstFighterOutfitted : secondFighterOutfitted;
          fatalDefender = (firstFighterAttacks && firstFighterOutfitted.availableFatalAttack) ? secondFighterOutfitted : firstFighterOutfitted;

          fatalAttacker.fatalAttack(fatalDefender);
          keysPressed.clear();
          if (secondFighterOutfitted.health < 0) {
            document.removeEventListener('keydown', handleAttack);
            document.removeEventListener('keyup', handleFatalAttack);
            resolve(firstFighterOutfitted);
          }
          if (firstFighterOutfitted.health < 0) {
            document.removeEventListener('keydown', handleAttack);
            document.removeEventListener('keyup', handleFatalAttack);
            resolve(secondFighterOutfitted);
          }

          fatalAttacker.availableFatalAttack = false;
          setTimer(fatalAttacker);
          await updateTimer(fatalAttacker);
        } 
      }

    })
}

function checkCombination(criticalHitCombination: Array<string>, pressedKeys: Set<string>) : Boolean {
  let countOfNeadedKeys = 0;
  for (let key of criticalHitCombination){
    if (pressedKeys.has(key)) 
      countOfNeadedKeys++;
  }
  if (criticalHitCombination.length == countOfNeadedKeys)
    return true;
  else 
    return false;
}
