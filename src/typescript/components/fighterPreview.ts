import { IFighterDetailed } from '../interfaces/IFighterDetailed';
import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter: IFighterDetailed, position: "left" | "right") {
    const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = <HTMLElement> createElement({
      tagName: 'div',
      className: `fighter-preview___root ${positionClassName}`,
    });
  
    if(fighter){
      const fighterElementInfo = <HTMLElement>createFighterInfo(fighter);
      fighterElement.appendChild(fighterElementInfo);

      const fighterElementImage = createFighterImage(fighter);
      fighterElement.appendChild(fighterElementImage);
    }
    return fighterElement;
  }

  function createFighterInfo(fighter: IFighterDetailed){
    const { name, attack, defense, health } = fighter;
    const attributes = {
      title: name,
    };
    const spanElement = createElement({
      tagName: 'span',
      className: 'fighter-preview___text',
      attributes,
    });
    const nameInfo = `name: ${name}`;
    const attackInfo = `attack: ${attack} `;
    const defenceInfo = `defense: ${defense} `;
    const healthInfo = `health: ${health}`;
    if (spanElement) {
        spanElement.appendChild(document.createTextNode(nameInfo));
        spanElement.appendChild(document.createElement("br"));
        spanElement.appendChild(document.createTextNode(attackInfo));
        spanElement.appendChild(document.createTextNode(defenceInfo));
        spanElement.appendChild(document.createTextNode(healthInfo));
    }   
    return spanElement;
  }

  export function createFighterImage(fighter: IFighterDetailed) {
    const { source, name } = fighter;
    const attributes = { 
      src: source, 
      title: name,
      alt: name 
    };
    const imgElement = createElement({
      tagName: 'img',
      className: 'fighter-preview___img',
      attributes,
    });
  
    return imgElement;
  }
  

