import { IFighterDetailed } from '../interfaces/IFighterDetailed';
import { fighterService } from '../services/fightersService';
import { createFighterPreview } from './fighterPreview';
import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resourses/versus.png';

export function createFightersSelector() {
    let selectedFighters: Array<IFighterDetailed> = [];
  
    return async (fighterId:string) => {
      const fighter : IFighterDetailed = <IFighterDetailed> await getFighterInfo(fighterId);
      const playerOne : IFighterDetailed | undefined = selectedFighters[0];
      const playerTwo: IFighterDetailed | undefined = selectedFighters[1];
      const firstFighter = playerOne || fighter;
      const secondFighter = Boolean(playerOne) ? playerTwo || fighter : playerTwo;
      selectedFighters = [firstFighter, secondFighter];
  
      renderSelectedFighters(selectedFighters);
    };
  }

  const fighterDetailsMap : Map<string, IFighterDetailed> = new Map<string, IFighterDetailed>();

  export async function getFighterInfo(fighterId: string) {
    let fighter : IFighterDetailed | string | undefined;
    if(!fighterDetailsMap.has(fighterId)){
      fighter = await fighterService.getFighterDetails(fighterId);
      fighterDetailsMap.set(fighterId, <IFighterDetailed>fighter);
    }
    else {
      fighter = fighterDetailsMap.get(fighterId);
    }
    return fighter;
  }

  function renderSelectedFighters(selectedFighters : Array<IFighterDetailed>) : void {
    const fightersPreview = <HTMLElement> document.querySelector('.preview-container___root');
    const [playerOne, playerTwo] = selectedFighters;
    const firstPreview = <HTMLElement> createFighterPreview(playerOne, 'left');
    const secondPreview = <HTMLElement> createFighterPreview(playerTwo, 'right');
    const versusBlock = <HTMLElement> createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  }

  function createVersusBlock(selectedFighters: Array<IFighterDetailed>) : HTMLElement {
    const canStartFight = selectedFighters.filter(Boolean).length === 2;
    const onClick = () => startFight(selectedFighters);
    const container = <HTMLElement> createElement({ tagName: 'div', className: 'preview-container___versus-block' });
    const image = <HTMLElement> createElement({
      tagName: 'img',
      className: 'preview-container___versus-img',
      attributes: { src: versusImg },
    });
    const disabledBtn : string = canStartFight ? '' : 'disabled';
    const fightBtn = <HTMLElement> createElement({
      tagName: 'button',
      className: `preview-container___fight-btn ${disabledBtn}`,
    });
    
    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';

    container.append(image, fightBtn);

    return container;
  }
  
  function startFight(selectedFighters: Array<IFighterDetailed>) : void {
    renderArena(selectedFighters);
  }