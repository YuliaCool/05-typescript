import App from '../../../app';
import { IFighterBattled } from '../../interfaces/IFighterBattled';

export function showWinnerModal(fighter: IFighterBattled) : void {
  const roundedHealth = Math.round(fighter.health * 100) / 100;
  const modalInfo = {
    title: `The winner is ${fighter.name}`, 
    bodyElement: `His health is ${roundedHealth} after fight. 
                  He has attacked ${fighter.countOfAttacks} times and ${fighter.countOfFatalAttack} times by fatal attack. 
                  He has got ${fighter.countOfFaults} hits from his opponent and dodged by ${fighter.countOfDodges} times.`,
    onClose: goToPreview
  };
  //showModal(modalInfo); - show perfect window with fighter info
  alert(`${modalInfo.title}. ${modalInfo.bodyElement}`);
  goToPreview();
}

export function goToPreview(){
  const div = <HTMLElement> document.getElementById("root");
  while(div.firstChild){
    div.removeChild(div.firstChild);
  }
  new App();
}