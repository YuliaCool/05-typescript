interface Element {
    tagName: string;
    className: string;
    attributes ?: {
        [key: string]: string;
    }

}
export function createElement({ tagName, className, attributes = {} }: Element ) : HTMLElement {
    
    const element = <HTMLElement> document.createElement(tagName);
  
    if (className) {
      const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }
    
    Object.keys(attributes).forEach((key: string) => element.setAttribute(key, attributes[key]));
  
    return element;
  }