import { IFighter } from '../interfaces/IFighter';
import { Fighter } from '../classes/Fighter';
import { IFighterDetailed } from '../interfaces/IFighterDetailed';
import { FighterDetailed } from '../classes/FighterDetailed';

export const fighters: Array<IFighter> = [
    new Fighter('1', 'Ryu', 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif'),
    new Fighter('2', 'Dhalsim', 'https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif'),
    new Fighter('3', 'Guile', 'https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif'),
    new Fighter('4', 'Zangief', 'https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif'),
    new Fighter('5', 'Ken', 'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif'),
    new Fighter('6', 'Bison', 'http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif')
  ];
  
  export const fightersDetails: Array<IFighterDetailed> = [
      new FighterDetailed('1', 'Ryu', 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif', 4, 3, 45),
      new FighterDetailed('2', 'Dhalsim', 'https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif', 3, 1, 60),
      new FighterDetailed('3', 'Guile', 'https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif', 4, 3, 45),
      new FighterDetailed('4', 'Zangief', 'https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif', 4, 1 ,60),
      new FighterDetailed('5', 'Ken', 'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif', 3, 4, 45),
      new FighterDetailed('6', 'Bison', 'http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif', 5, 4, 45),
  ];
  