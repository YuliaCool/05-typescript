import { IFighterDetailed } from "../interfaces/IFighterDetailed";

export interface IFighterBattled extends IFighterDetailed {
    initialHealth: number;
    blocked: boolean;
    availableFatalAttack: boolean;
    position: 'right' | 'left';
    countOfAttacks: number;
    countOfFatalAttack: number;
    countOfFaults: number;
    countOfDodges: number;

    attackAction(defender: IFighterBattled) : void;
    blockAction() : void;
    getDamage(defender: IFighterBattled) : number;
    getRoundedDamage(damage: number): number;
    getFatalDamage() : number;
    getHitPower() : number;
    getBlockPower() : number;
    fatalAttack(defender: IFighterBattled) : Promise<void>;
}