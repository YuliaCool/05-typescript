import { IFighter } from "../interfaces/IFighter";

export interface IFighterDetailed extends IFighter {
    attack: number;
    defense: number;
    health: number; // readonly
}