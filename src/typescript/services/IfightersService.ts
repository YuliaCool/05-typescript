import { IFighter } from '../interfaces/IFighter';
import { IFighterDetailed } from '../interfaces/IFighterDetailed';

export interface IFighterService {
    getFighters () : Promise<Array<IFighter> | string | undefined>;
    getFighterDetails(id: string) : Promise<IFighterDetailed | string | undefined>;

}