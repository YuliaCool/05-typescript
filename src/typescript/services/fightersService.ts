import { IFighterService } from './IfightersService';
import { IFighter } from '../interfaces/IFighter';
import { IFighterDetailed } from '../interfaces/IFighterDetailed';
//import { IErrorHandling } from './IErrorHandling';
import { callApi } from '../helpers/apiHelper';

// type FightersResponce = IFighter[] & IErrorHandling;
// type FighterDetailedResponce = IFighterDetailed & IErrorHandling;

class FighterService implements IFighterService {

    getFighters = async (): Promise<Array<IFighter> | string | undefined> => {
      try {
        const endpoint : string = 'fighters.json';
        const apiResult : IFighter[] = await callApi(endpoint, 'GET');
  
        return apiResult;
      } catch (error) {
        if(error) {
            return error.message;
        }
      }
    }
  
    getFighterDetails = async (id: string) : Promise<IFighterDetailed | string | undefined> => {
      try {
        const endpoint : string = `details/fighter/${id}.json`;
        const apiResult : IFighterDetailed = await callApi(endpoint, 'GET');
  
        return apiResult;
      } catch (error) {
        if(error) {
            return error.message;
        }
      }
    }
  }
  
  export const fighterService: IFighterService = new FighterService();